<?php
//view errors 
// ini_set('display_errors',1);
// ini_set('display_startup_errors',1);
// error_reporting(-1);

//require_once 'scoring.php';

//declare our variables
$name = $_GET['name'];
$age = $_GET['age'];
$gender = $_GET['gender'];
$email = $_GET['email'];
$way = $_GET['way'];
$time = $_GET['time'];
$phone = $_GET['phone'];


//get value for scored elements
//normal scored
$q3 = $_GET['element_139'];
$q6 = $_GET['element_136'];
$q8 = $_GET['element_134'];
$q13 = $_GET['element_129'];
$q19 = $_GET['element_123'];
$q23 = $_GET['element_119'];
$q28 = $_GET['element_114'];
$q29 = $_GET['element_113'];
$q34 = $_GET['element_108'];
$q36 = $_GET['element_106'];
$q37 = $_GET['element_105'];
$q48 = $_GET['element_94'];
$q51 = $_GET['element_91'];
$q57 = $_GET['element_84'];
$q58 = $_GET['element_83'];
$q60 = $_GET['element_81'];
$q69 = $_GET['element_72'];
$q75 = $_GET['element_66'];
$q79 = $_GET['element_62'];
$q84 = $_GET['element_57'];
$q88 = $_GET['element_53'];
$q89 = $_GET['element_52'];
$q92 = $_GET['element_49'];
$q95 = $_GET['element_46'];
$q101 = $_GET['element_40'];
$q105 = $_GET['element_36'];
$q108 = $_GET['element_33'];
$q115 = $_GET['element_26'];
$q124 = $_GET['element_17'];
$q129 = $_GET['element_12'];
$q136 = $_GET['element_5'];
//reverse scored
$q16 = $_GET['element_126'];
$q25 = $_GET['element_117'];
$q45 = $_GET['element_97'];
$q64 = $_GET['element_77'];
$q112 = $_GET['element_29'];
$q118 = $_GET['element_23'];

//--------------------------------------------SCORING AND EMAIL----------------------------------------

//multidimentional array of all potentially scored questions, all normal and all reverse scored
$allQ = array(
	"normal" => array(		
		$q3,
		$q6,
		$q8,
		$q13,
		$q19,
		$q23,
		$q28,
		$q29,
		$q34,
		$q36,
		$q37,
		$q48,
		$q51,
		$q57,
		$q58,
		$q60,
		$q69,
		$q75,
		$q79,
		$q84,
		$q88,
		$q89,
		$q92,
		$q95,
		$q101,
		$q105,
		$q108,
		$q115,
		$q124,
		$q129,
		$q136,),
	"reversed" => array($q16, $q25, $q45, $q64, $q112, $q118),
);

$x = 0;//number of 2 point questions
$rawScore = 0;//sum of points

//scoring implimentation
foreach($allQ as $type => $nestedArray){//for each nestedArray in allQ
	foreach($nestedArray as $question => $answer){//for each qeustion and value in nestedArray
		if($type === "reversed"){
			switch ($answer){
				case 1://REVERSED YES answer
					$rawScore += 1;
					break;
				case 2://REVERSED NO answer
					$x += 1;//add one to variable x if answer was no
					$rawScore += 2; //add 2 to sum of points
					break;
			}
		} 
		else {//if normal question type
			switch($answer){
				case 2://NORMAL YES answer
					$x += 1; // add one to x variable
					$rawScore += 2; // add 2 to sum of points
					break;
				case 1://NORMAL NO answer
					$rawScore += 1;
					break;
			}
		}	
	}
}

$fsqScore = round((($x + 37) / 37), 2);//calculated score, rounded to nearest hundreth


//---------------------------------------------SEND EMAIL-----------------------------------------------
$message =

"\r \n 
Name: $name
Age: $age 
Gender: $gender 
Email: $email 
Phone: $phone 
Easiest Way to reach: $way 
Best Time To Reach you: $time
Raw Score: $rawScore
FSQ Score: $fsqScore

Normally Scored Responses(Yes = 2, No = 1):
Q3: $q3
Q6: $q6
Q8: $q8
Q13: $q13
Q19: $q19
Q23: $q23
Q28: $q28
Q29: $q29
Q34: $q34
Q36: $q36
Q37: $q37
Q48: $q48
Q51: $q51
Q57: $q57
Q58: $q58
Q60: $q60
Q69: $q69
Q75: $q75
Q79: $q79
Q85: $q84
Q88: $q88
Q89: $q89
Q92: $q92
Q95: $q95
Q101: $q101
Q105: $q105
Q108: $q108
Q115: $q115
Q124: $q124
Q129: $q129
Q136: $q136

Reverse Scored Responses(Yes = 1, No = 2):
Q16: $q16 
Q25: $q25 
Q45: $q45 
Q64: $q64 
Q112: $q112
Q118: $q118
";

//put recipient address here
$to = "siegellab08@gmail.com";




$headers = "From: $email";
$subject = "FSQ Score: ($fsqScore)";
$sent = mail($to, $subject, $message, $headers);

// Redirect to this page after submitting survey
$newURL = "http://thesiegellab.com/thankYou.html";
header("Location: " . $newURL);
die();
?>



